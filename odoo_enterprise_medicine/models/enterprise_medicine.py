from odoo import api, models


class EnterpriseMedicine(models.AbstractModel):
    _name = 'enterprise.medicine'

    @api.model
    def inject_medicine(self):
        # disable check lisence cron job
        self.env.ref('mail.ir_cron_module_update_notification').sudo().write({'active': False})

        # rewrite expiration date
        IrConfig = self.env['ir.config_parameter'].sudo()
        IrConfig.set_param('database.expiration_date', '2100-01-01 00:00:00')
